package com.example.silvia.converter2;
import android.renderscript.Double2;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
public class MainActivity extends AppCompatActivity {
    Button button1,button2;
    EditText editText1,editText2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText1=(EditText)findViewById(R.id.txt1);
        button1= (Button) findViewById(R.id.convert_btn);
        button2= (Button) findViewById(R.id.exit_btn);
        editText2=(EditText)findViewById(R.id.text2);
        button1.setOnClickListener(new View.OnClickListener() {

                                       public void onClick(View view) {
                                           Double milimeter = Double.parseDouble(editText1.getText().toString());
                                           Double result;
                                           result = (milimeter / 25.4);
                                           editText2.setText(Double.toString(result));

                                       }
                                   }
        );
        button2.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view){
                finish();
            }
        });
    }
}
